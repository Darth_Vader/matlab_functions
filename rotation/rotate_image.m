function v = rotate_image(img,angle)
	
	[width,height] = size(img);
	raster = transpose(img);
	%% shift origin
	
	new_img = zeros(width,height);
	
	for ii=1:height
		for jj=1:width
			[x,y] = raster_to_cart(ii,jj,width,height);
			[x_rotated,y_rotated] = rotate(x,y,angle);
			
		end
	end
	

end
