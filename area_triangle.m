function area = area_triangle(a1,b1,a2,b2,a3,b3)

%	X = sprintf('\nPoint are %d,%d\t%d,%d\t%d,%d\n',a1,b1,a2,b2,a3,b3);	
%	disp(X);
	a = sqrt((a1-a2)*(a1-a2) + (b1-b2)*(b1-b2));
	b = sqrt((a1-a3)*(a1-a3) + (b1-b3)*(b1-b3) );
	c = sqrt((a2-a3)*(a2-a3) + (b2-b3)*(b2-b3));

	s = (a+b+c)/2;

	area = sqrt(s*(s-a)*(s-b)*(s-c));
%	X = sprintf('Area of triangle is : %f\n',area);
%	disp(X);

end
