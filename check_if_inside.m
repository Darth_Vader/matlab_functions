function y = check_if_inside(x,y,a1,b1,a2,b2,a3,b3,a4,b4)

	ar1 = area_triangle(x,y,a1,b1,a2,b2);
	ar2 = area_triangle(x,y,a2,b2,a3,b3);
	ar3 = area_triangle(x,y,a3,b3,a4,b4);
	ar4 = area_triangle(x,y,a4,b4,a1,b1);
	
	rect_area = area_triangle(a1,b1,a2,b2,a4,b4)+ area_triangle(a2,b2,a4,b4,a3,b3);

	if abs(rect_area - (ar1+ar2+ar3+ar4)) < 0.000001
		y = 1;
	else
		y = 0;
	end

end
