function [X,Y] = rotate_point(x,y,angle)

X = x*cos(3.14/180 * angle) - y*sin(3.14/180 * angle);
Y = x*sin(3.14/180 * angle) + y*cos(3.14/180*anlge);

end
