function v = nearest(x_,y_,mat)
	[x,y] = size(mat);
	t = y_;
	y_ = x_ + 1;
	x_ = x - t;

	X = 1;
	Y = 1;
	dist = (x_-1)*(x_-1) + (y_-1)*(y_-1);
	for i=1:x
		for j=1:y
			temp_dist = (x_-i)*(x_-i) + (y_-j)*(y_-j);
			if temp_dist < dist
				dist = temp_dist;
				X = i;
				Y = j;
			end
		end
	end
	v = mat(X,Y);
end
