function y = reduce_size(mat,n)

y = zeros(n,n,'uint8');

for i = 1:n
    for j=1:n
        y(i,j,1) = ( mat(i*2,j*2) + mat(i*2-1,j*2) + mat(i*2,j*2-1) + mat(i*2-1,j*2-1))/4;
    end
end

end


function [x1,x2,y] = xxx(image, angle)

        [m,n] = size(image);
        x1 = m*cos(3.14 /2 - 3.14 / 180 * angle);
        x2 = m*cos(3.14/180*angle);
        y = x1 + x2;
        x1 = x1*-1;
end

