function img = rotate_image(image,angle)

	[x_,y_] = size(image);
	[x1,x2,y,a1,b1,a2,b2,a3,b3,a4,b4] = new_dimensions(image,angle);
    	
	img = zeros(y,y);

	for i=1:y
		for j=1:y
			X = sprintf('\nChecking point %d,%d of matrix and  %d,%d in cart system',i,j,j+x1-1,y-i);
			disp(X);
			if check_if_inside(j+x1-1,y-i,a1,b1,a2,b2,a3,b3,a4,b4)
				X = sprintf('\nPoint is inside');
				disp(X);
    				[x__,y__] = rotate_counter(j+x1-1,y-i,angle);
				X = sprintf('\nRotated point is : %d,%d',x__,y__);
				disp(X);
				img(i,j) = nearest(x__,y__,image);
				%img(i,j)=1;
			else
				img(i,j) = 0;
			end
		end
	end

end
