function v = nearest(x_,y_,mat)
	[x,y] = size(mat);
%	t = y_;
%	y_ = x_ + 1;
%	x_ = x - t;
	[xx,yy] = size(mat);
	X = 1;
	Y = 1;
	dist = (x_- Y + 1)*(x_- Y + 1) + (y_- xx + X)*(y_- xx + X);
	for i=1:x
		for j=1:y
			temp_dist = (x_-j + 1)*(x_- j + 1) + (y_-xx+i)*(y_-xx+i);
			if temp_dist < dist
				dist = temp_dist;
				X = i;
				Y = j;
			end
		end
	end
	v = mat(X,Y);
	tt = sprintf('\nNearest neighbour of %d,%d is %d,%d',x_,y_,X,Y);
	disp(tt);
end
