function [x1,x2,y,a1,b1,a2,b2,a3,b3,a4,b4] = new_dimensions(image, angle)

        [m,n] = size(image);
        x1 = m*cos(3.14 /2 - 3.14 / 180 * angle);
        x2 = m*cos(3.14/180*angle);
        y = x1 + x2;
        x1 = x1*-1;
	x2 = floor(x2);
	x1 = ceil(x1);
	y = ceil(y);

	a1 = round( -1* n*sin(3.14/180 * angle));
	b1 = round(n*cos(3.14/180*angle));
	%b1 = a1 - x1 + 1;
	%a1 = y - round(n*cos(3.14/180 * angle));

	a2 = round(m*cos(3.14/180 * angle) - n*sin(3.14/180*angle));
	b2 = round(m*sin(3.14/180 * angle) + n * cos(3.14/180*angle));
	%b2 = a2 - x1 + 1;
	%a2 = y - round(m*sin(3.14/180 * angle) + n * cos(3.14/180*angle));

	a3 = round(m *cos(3.14/180*angle));
	b3 =  round(m * sin(3.14/180 * angle));
	%b3 = a3 - x1 + 1;
	%a3 = y - round(m * sin(3.14/180 * angle));

	b4 = -x1 + 1;
	a4 = y;

	b4 = 0;
	a4 = 0;

	

end

