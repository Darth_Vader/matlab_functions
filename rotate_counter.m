function [X,Y] = rotate_counter(x,y,angle)


X = x*cos(3.14/180 * angle) + y*sin(3.14/180 * angle);
Y = -1*x*sin(3.14/180 * angle) + y*cos(3.14/180*angle);

end
