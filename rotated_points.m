function x = rotate_points(p,angle)

	[a,b] = size(p);
	x = zeros(a,2);
	for i=1:a
		for j=1:b
			x[i][j] = rotate_point(p[i][j],angle);
		end
	end
end
